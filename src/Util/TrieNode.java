/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author FELIPE ALFEREZ VILLAMIZAR - 1151902
 */
public class TrieNode {
    private TablaHash<String , TrieNode> children;
    private boolean end;
    
    public TrieNode(){
        this.end = false;
        this.children = new TablaHash();
    }

    public TablaHash<String, TrieNode> getChildren(){
        return this.children;
    }
    
    public boolean getEnd(){
        return this.end;
    }
    
    public void setEnd(boolean end){
        this.end = end;
    }
    
}
