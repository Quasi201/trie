/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.text.Normalizer;

/**
 * @author FELIPE ALFEREZ VILLAMIZAR - 1151902
 */
public class Trie {
    private TrieNode root;
    private String digrafos[]= {"ch","gu","ll","rr","qu"};    
    
    public Trie(String url){
        root = new TrieNode();
        cargarTrie(url);
    }
    
    private void cargarTrie(String url){
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();
        String words[] = v[0].toString().split(", ");
        for (String word : words) {
            add(word);
        }
    }
    
    public void add(String word){
        int limit =  word.length();
        TrieNode actual = root;
        word = this.normalize(word);
        for(int i=0; i<limit ;){
            String ch = ((Character)word.charAt(i)).toString();
            String dig = i!=limit-1?word.substring(i, i+2) : ch;
            TrieNode next;
            if(isDigrafo(dig)){
                next = actual.getChildren().esta(dig);
                if(next == null){
                    next = new TrieNode();
                    actual.getChildren().insertar(dig, next);
                }
                i+=2;
            }
            else{
                next = actual.getChildren().esta(ch);
                if(next == null){
                    next = new TrieNode();
                    actual.getChildren().insertar(ch, next);
                }
                i++;
            }
            actual = next;
        }
        actual.setEnd(true);
    }
    
    public boolean search(String word){
        TrieNode actual = this.root;
        word = this.normalize(word);
        int limit = word.length();
        for(int i=0; i<limit;){
            String ch = ((Character)word.charAt(i)).toString();
            String dig = i!=limit-1?word.substring(i, i+2) : ch;
            TrieNode next;
            if(isDigrafo(dig)){
                next = actual.getChildren().esta(dig);
                i+=2;
            }
            else{
                next = actual.getChildren().esta(ch);
                i++;
            }
            if(next==null)return false;
            actual = next;
        }
        return true;
    }
    
    public boolean isDigrafo(String dig){
        for(String it : digrafos){
            if(dig.equals(it))return true;
        }
        return false;
    }
    public String[] splitWord(String word){
        String aux=new String();
        int limit = word.length();
        word = this.normalize(word);
        for(int i=0; i<limit;){
            String ch = ((Character)word.charAt(i)).toString();
            String dig = i!=limit-1?word.substring(i, i+2) : ch;
            if(isDigrafo(dig)){
                aux+=dig;
                i+=2;
                
            }
            else{
                aux+=ch;
                i++;
            }
            aux+="-";
        }
        return aux.split("-");
    }
    public void printAllWords(String word, TrieNode next){
        if(next==null)return;
        ListaCD<InformacionDeEntrada<String, TrieNode>>act[] = next.getChildren().getInformacionEntrada();
        for (ListaCD<InformacionDeEntrada<String, TrieNode>> list : act) {
            for (InformacionDeEntrada<String, TrieNode> info : list) {
                String aux = info.getClave();
                TrieNode tmp = info.getObjeto(), tn = next.getChildren().esta(aux);    
                int t = tn.getChildren().getNumeroDatos();
                if(tmp.getEnd()) System.out.println(word+aux);
                printAllWords(word+aux, next.getChildren().esta(aux));
            }   
        }
    }
    
    public TrieNode getRoot(){
        return this.root;
    }
    
     public String normalize(String s){
        return Normalizer.normalize(s.toLowerCase(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
    
}
