/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.InformacionDeEntrada;
import Util.ListaCD;
import Util.TrieNode;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;


public final class TrieToPdf {
    public final Color OCEAN = new DeviceRgb(140, 245, 240);//Color de un nodo fin de palabra
    public final Color BLUE = new DeviceRgb(84, 190, 255);//Color de un nodo
    public final Color BLACK = new DeviceRgb(0, 0, 0);//Color de la letra
    public final Color YELLOW = new DeviceRgb(255, 242, 145);//Color de un nodo marcado(Busqueda de una palabra)
    private float radio = 20;
    private int sizeFont = 12;

    public TrieToPdf(){}
    
    public TrieToPdf(TrieNode root, boolean puede, String splitWord[]){
        JOptionPane.showMessageDialog(null, "Si su palabra no esta dentro del trie no se generara un segundo pdf :(");
        this.crearPdf(root);
        if (puede){
            this.crearPdfMarcado(root, splitWord);
        }
        else JOptionPane.showMessageDialog(null, "Su palabra no se encontro en el trie, intentelo de nuevo :)");
    }

    private void crearPdf(TrieNode root){
        try {
            PdfWriter writer = new PdfWriter("d:/vistaTrie.pdf");
            PdfDocument pdf = new PdfDocument(writer);
            PageSize ps = PageSize.A2;
            PdfPage page = pdf.addNewPage(ps);
            PdfCanvas node = new PdfCanvas(page);
            PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA);
            node.setFontAndSize(font, sizeFont);
            float x = ps.getWidth() / 2, y = ps.getHeight();
            
            node.beginText();
                pdf(root, node, "", x, y - radio, x, y + radio, root.getChildren().getNumeroDatos(), true);
            node.endText();
            
            pdf.close();
            File fichero = new File("d:/vistaTrie.pdf");
            Desktop.getDesktop().open(fichero);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println("El pdf esta lista espere un momento mientras se abre el documento");
    }
    
    private void crearPdfMarcado(TrieNode root, String word[]) {
        try {
            PdfWriter writer = new PdfWriter("d:/vistaTrieMarcado.pdf");
            PdfDocument pdf = new PdfDocument(writer);
            PageSize ps = PageSize.A2;
            PdfPage page = pdf.addNewPage(ps);
            PdfCanvas node = new PdfCanvas(page);
            PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA);
            node.setFontAndSize(font, sizeFont);
            float x = ps.getWidth() / 2, y = ps.getHeight();
            
            node.beginText();
                pdfMarcado(root, node, "", word, x, y - radio, x, y + radio, root.getChildren().getNumeroDatos(), -1, true,0);
            node.endText();
            
            pdf.close();
            File fichero = new File("d:/vistaTrieMarcado.pdf");
            Desktop.getDesktop().open(fichero);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println("El pdf esta lista espere un momento mientras se abre el documento");
    }
    
    private void pdf(TrieNode actual, PdfCanvas node, String let, float x, float y, float padreX, float padreY, int h, boolean isRoot){
        if (actual == null) return;
        this.dibujarNodo(node, let, actual.getEnd(), x, y, padreX, padreY, false);
        ListaCD<InformacionDeEntrada<String, TrieNode>> list = this.unificar(actual.getChildren().getInformacionEntrada());
        h = isRoot && h > 1 ?  5 : list.getTamanio();
        float left = getPlace(h)/2.0f;
        float rigth=getPlace(h)/(list.getTamanio()-1);
        for (InformacionDeEntrada<String, TrieNode> it : list){            
            String s = it.getClave();
            if(h>1){
                pdf(actual.getChildren().esta(s), node, s, x-left, y - radio * 4.0f, x, y, h, false);
                left-=rigth;
            }
            else pdf(actual.getChildren().esta(s), node, s, x, y - radio * 4.0f, x, y, h, false);
        }
    }
    
    private void pdfMarcado(TrieNode actual, PdfCanvas node, String let,String vec[], float x, float y, float padreX,float padreY, int h, int index, boolean isRoot, int cnt){
        if (actual == null) return;
        boolean inRange = index>=0 && index<vec.length;
        boolean especial = inRange && let.equals(vec[index]);
        index++;
        if(especial)cnt++;
        this.dibujarNodo(node, let, actual.getEnd(), x, y, padreX, padreY, especial && cnt == index);
        ListaCD<InformacionDeEntrada<String, TrieNode>> list = this.unificar(actual.getChildren().getInformacionEntrada());
        h = isRoot && h > 1 ?  5 : list.getTamanio();//Si se añaden mas palabras, aumentar el numero de la opcionSI del ternario
        float left = getPlace(h)/2.0f;
        float rigth=getPlace(h)/(list.getTamanio()-1);
        for (InformacionDeEntrada<String, TrieNode> it : list){            
            String s = it.getClave();
            if(h>1){
                pdfMarcado(actual.getChildren().esta(s), node, s, vec, x-left, y - radio * 4.0f, x, y, h, index, false,cnt);
                left-=rigth;
            }
            else pdfMarcado(actual.getChildren().esta(s), node, s, vec, x, y - radio * 4.0f, x, y, h, index, false,cnt);
        }
    }
    
        
    private ListaCD<InformacionDeEntrada<String, TrieNode>> unificar(ListaCD<InformacionDeEntrada<String, TrieNode>> l[]) {
        ListaCD<InformacionDeEntrada<String, TrieNode>> tmp = new ListaCD();
        for (ListaCD<InformacionDeEntrada<String, TrieNode>> list : l) {
            for (InformacionDeEntrada<String, TrieNode> info : list) {
                tmp.insertarOrdenado(info);
            }
        }
        return tmp;
    }
    
    private void dibujarNodo(PdfCanvas node, String let, boolean isEnd, float x, float y, float parentX, float parentY, boolean marcar) {
        if (marcar) {
            node.circle(x, y, radio).setColor(YELLOW, true).fillStroke();
        } else if (isEnd) {
            node.circle(x, y, radio).setColor(BLUE, true).fillStroke();
        } else {
            node.circle(x, y, radio).setColor(OCEAN, true).fillStroke();
        }
        node.moveTo(x, y + radio).lineTo(parentX, parentY - this.radio).stroke();
        node.setTextMatrix(x, y);
        node.setColor(BLACK, true).showText(let).setColor(OCEAN, true);
    }
    
    private float getPlace(float n) {
        return (n * this.radio * 4.0f) - 1.0f;
    }
}
