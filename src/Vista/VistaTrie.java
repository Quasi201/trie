/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Trie;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author FELIPE
 */
public class VistaTrie {

    public static void main(String[] args) {
        Trie t = new Trie("http://madarme.co/persistencia/palabras.txt");
        Scanner sc = new Scanner(System.in);
        JOptionPane.showMessageDialog(null, "Al insertar la palabra se generaran y abriran 2 archivos pdfs los cuales son:\n1. Un pdf con la visualizacion del trie\n2.Un pdf con la visualizacion del trie y la palabra a buscar resaltada");
        System.out.print("Ingrese la palabra a buscar en el trie: ");
        String s = sc.next();
        TrieToPdf pdf = new TrieToPdf(t.getRoot(), t.search(s), t.splitWord(s));
    }
}
 